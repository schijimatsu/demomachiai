#!/bin/bash

ENV_ROOT=$1

pkill -HUP -f `basename ${ENV_ROOT}/bin/gunicorn`
