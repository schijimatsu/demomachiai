#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Created on 2014-03-07
@author chijimatsu
'''

import logging
import logging.config
import os, sys, re
import codecs
logger = None
from pkg_resources import (
  resource_filename, 
)
from optparse import OptionParser
from configparser import ConfigParser, SafeConfigParser
from paste.deploy.loadwsgi import NicerConfigParser
from paste.deploy import appconfig

def rename(name, before, after):
  if os.path.basename(name).find(before) != -1:
    logger.warn('found name: %s, %s' %(name, before))
    os.rename(
      name, 
      os.path.join(
        os.path.dirname(name), 
        re.sub(
          '^(.*?)(%s)(.*)$' % before, 
          '\\1%s\\3' % after, 
          os.path.basename(name), 
        )
      )
    )

def replace(path, target, to, backup=None, pattern=None, encoding='utf8'):
  if (pattern and re.compile(pattern).match(os.path.basename(path))) or pattern is None:
    content = ''
    replaced = ''
    matched = False
    with codecs.open(path, 'r', encoding) as f:
      for line in f.readlines():
        content += line
        if line.find(target) != -1:
          matched = True
          replaced_line = re.sub(target, to, line, count=0)
          replaced += replaced_line
          logger.warn(line + 'at %s, replaced: %s' %(path, replaced_line))
        else:
          replaced += line

    if matched:
      with codecs.open(path, 'w', encoding) as f:
        f.write(replaced)
  
      if backup:
        with codecs.open(path+'.%s' % backup, 'w', encoding) as f:
          f.write(content)

def rename_and_replace(this_project_name, path='.', template_project_name='TemplateMachiai'):
  for root, dirs, files in os.walk(path):
    # logger.warn('root, dirs, files: %s, %s, %s' %(root, dirs, files))
    if re.compile('^.*?\\.[^/].+$').match(root) is None:
      for file in files:
        if file != __file__ and not re.match(r'^.+\.(png|jpg|gif|pdf|pyc|ignore|bak)$', file, re.I):
          joined_path = os.path.join(root, file)
          # logger.warn(joined_path)
          replace(joined_path, template_project_name.lower(), this_project_name.lower(), 'bak')
          replace(joined_path, template_project_name, this_project_name, 'bak')
          rename(joined_path, template_project_name.lower(), this_project_name.lower())
          rename(joined_path, template_project_name, this_project_name)

  for root, dirs, files in os.walk(path):
    # logger.warn('root, dirs, files: %s, %s, %s' %(root, dirs, files))
    if re.compile('^.*?\\.[^/].+$').match(root) is None:
      for dir in dirs:
        logger.warn('dir: %s' % dir)
        if dir.find('.') != 0:
          joined_path = os.path.join(root, dir)
          logger.warn(joined_path)
          rename(joined_path, template_project_name.lower(), this_project_name.lower())
          rename(joined_path, template_project_name, this_project_name)

      # root_path = os.path.abspath(root)
      # if re.match(r'^/.+/[^\.]+$', root_path) is not None:
      #   logger.warn('root_path: %s' % root_path)
      #   rename(root_path, template_project_name.lower(), this_project_name.lower())
      #   rename(root_path, template_project_name, this_project_name)

if __name__ == '__main__':
  parser = OptionParser()
  parser.add_option("--config", dest="config_filename", help="pass the path/to/config", metavar="FILE")
  
  (options, args) = parser.parse_args()

  if options.config_filename:
    logging.config.fileConfig(os.path.abspath(options.config_filename))
    logger = logging.getLogger(__package__)

    rename_and_replace(args[0], **({'template_project_name': args[1]} if len(args) > 1 else {}))
