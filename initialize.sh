#!/bin/sh

rm DemoMachiai.sqlite

initialize_DemoMachiai_db development.ini

python -m machiai.scripts.otc_loader --conf=development.ini --pdf=../Machiai/machiai/static/medicine/otc/pdf --img=../Machiai/machiai/static/medicine/otc/img ../../data/otc_datas.txt

python -m machiai.fixture.loader -c development.ini -f ../Machiai/machiai/fixture/related_terms.json
python -m machiai.fixture.loader -c development.ini -f ../Machiai/machiai/fixture/terms.json
