ROOT=$1
PROJECT=$2Machiai
PROJECT_LOWER=`echo $PROJECT | tr '[:upper:]' '[:lower:]'`

cd $ROOT

git clone git@bitbucket.org:schijimatsu/template_machiai.git $PROJECT
cd $PROJECT
curl --request POST --user schijimatsu:on1socwm4x https://api.bitbucket.org/1.0/repositories/ --data name=$PROJECT --data scm=git
git remote set-url origin git@bitbucket.org:schijimatsu/$PROJECT.git
python pre_setup.py --config=development.ini $PROJECT
git add $PROJECT_LOWER
git commit -a -m 'initial commitment.'
git checkout -b develop
git push origin develop
