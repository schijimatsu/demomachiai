(function() {
  "use strict";
  var HospitalTerm, HospitalTerms, Medicine, MedicineCategories, MedicineCategory, Medicines,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Medicine = (function(_super) {
    __extends(Medicine, _super);

    function Medicine() {
      return Medicine.__super__.constructor.apply(this, arguments);
    }

    Medicine.prototype.idAttribute = 'id';

    return Medicine;

  })(Backbone.Model);

  MedicineCategory = (function(_super) {
    __extends(MedicineCategory, _super);

    function MedicineCategory() {
      return MedicineCategory.__super__.constructor.apply(this, arguments);
    }

    MedicineCategory.prototype.idAttribute = 'id';

    return MedicineCategory;

  })(Backbone.Model);

  HospitalTerm = (function(_super) {
    __extends(HospitalTerm, _super);

    function HospitalTerm() {
      return HospitalTerm.__super__.constructor.apply(this, arguments);
    }

    HospitalTerm.prototype.idAttribute = 'id';

    return HospitalTerm;

  })(Backbone.Model);

  Medicines = (function(_super) {
    __extends(Medicines, _super);

    function Medicines() {
      return Medicines.__super__.constructor.apply(this, arguments);
    }

    Medicines.prototype.model = Medicine;

    Medicines.prototype.url = '/medicine';

    return Medicines;

  })(Backbone.Collection);

  MedicineCategories = (function(_super) {
    __extends(MedicineCategories, _super);

    function MedicineCategories() {
      return MedicineCategories.__super__.constructor.apply(this, arguments);
    }

    MedicineCategories.prototype.model = MedicineCategory;

    MedicineCategories.prototype.url = '/medicine_category';

    return MedicineCategories;

  })(Backbone.Collection);

  HospitalTerms = (function(_super) {
    __extends(HospitalTerms, _super);

    function HospitalTerms() {
      return HospitalTerms.__super__.constructor.apply(this, arguments);
    }

    HospitalTerms.prototype.model = HospitalTerm;

    HospitalTerms.prototype.url = '/hospital_term';

    return HospitalTerms;

  })(Backbone.Collection);

}).call(this);
