#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Created on 2014-03-07
@author chijimatsu
'''

from pyramid.response import Response
from pyramid.view import view_config

from sqlalchemy.exc import DBAPIError

from machiai.models import (
  DBSession,
)

from machiai.views import before_process

@view_config(route_name='home', renderer='demomachiai:templates/index.haml')
@before_process
def home(context, request):
  return {}

