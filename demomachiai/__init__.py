# from pyramid.config import Configurator
# from sqlalchemy import engine_from_config

# from .models import (
#     DBSession,
#     Base,
#     )

import machiai

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    settings = machiai.set_default_settings(settings)
    machiai.configure_db_wrapper(settings)
    config = machiai.new_config(settings)
    machiai.configure_template_engine(config, settings)
    config.override_asset(
      to_override='machiai:templates/', 
      override_with='demomachiai:templates/', 
    )
    config.override_asset(
      to_override='machiai:static/', 
      override_with='demomachiai:static/', 
    )
    config.include(machiai.add_subscriber)
    config.include(machiai.configure_routes)
    machiai.for_debug(config, settings)
    config.scan('machiai')
    config.commit()

    config.scan()
    return config.make_wsgi_app()
